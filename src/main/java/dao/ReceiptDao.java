/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import model.Product;
import model.Receipt;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.ReceiptDetail;
import model.User;


/**
 *
 * @author Patcharawalai
 */
public class ReceiptDao implements DaoInterface<Receipt> {

    @Override
    public int add(Receipt object) {
        PreparedStatement stmt = null;
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO receipt (customer_id,user_id,total) VALUES (?,?,?);";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getCustomer().getId());
            stmt.setInt(2, object.getSeller().getId());
            stmt.setDouble(3, object.getTotal());
            int row = stmt.executeUpdate();
            ResultSet rs = stmt.getGeneratedKeys();

            if (rs.next()) {
                id = rs.getInt(1);
                object.setId(id);
            }
            for(ReceiptDetail r : object.getReceiptDetail()){
                String sqlDetail = "INSERT INTO receipt_detail (receipt_id,product_id,price,amount) VALUES (?,?,?,?);";
                PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
                stmtDetail.setInt(1,r.getReceipt().getId());
                stmtDetail.setInt(2, r.getProduct().getId());
                stmtDetail.setDouble(3, r.getPrice());
                stmtDetail.setInt(4, r.getAmount());
                int rowDetail = stmt.executeUpdate();
                ResultSet rsDetail = stmt.getGeneratedKeys();
                
                if (rsDetail.next()) {
                    id = rsDetail.getInt(1);
                    r.setId(id);
                }
            }
        } catch (SQLException ex) {
            System.out.println("Can't open database! :(");
            System.exit(0);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Receipt> getAll() {
        ArrayList list = new ArrayList();
        Statement stmt = null;
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT r.id as id,\n" +
                        "       r.created as created,\n" +
                        "       customer_id,\n" +
                        "       c.name as customer_name,\n" +
                        "       c.tel as customer_tel,\n" +
                        "       user_id,\n" +
                        "       u.name as user_name,\n" +
                        "       u.tel as user_tel,\n" +
                        "       total    \n" +
                        "  FROM receipt r,customer c , user u\n" +
                        "  WHERE r.customer_id=c.id AND r.user_id=u.id"+
                        "  ORDER BY created DESC;";
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                int id = rs.getInt("id");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rs.getString("created"));
                int customerId = rs.getInt("customer_id");
                String customerName = rs.getString("customer_name");
                String customerTel = rs.getString("customer_tel");
                int userId = rs.getInt("user_id");
                String userName = rs.getString("user_name");
                String userTel = rs.getString("user_tel");
                double total = rs.getDouble("total");
                Receipt receipt = new Receipt(id, created,
                        new User(userId,userName,userTel),new Customer(customerId,customerName,customerTel));
                list.add(receipt);
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.out.println("Can't open database! :(");
            System.exit(0);
        } catch (ParseException ex) {
            System.out.println("Date error "+ex);
        }
        db.close();
        return list;
    }

    @Override
    public Receipt get(int id) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT r.id as id,\n"
                    + "       r.created as created,\n"
                    + "       customer_id,\n"
                    + "       c.name as customer_name,\n"
                    + "       c.tel as customer_tel,\n"
                    + "       user_id,\n"
                    + "       u.name as user_name,\n"
                    + "       u.tel as user_tel,\n"
                    + "       total\n"
                    + "  FROM receipt r,customer c, user u\n"
                    + "  WHERE r.id = ? AND r.customer_id = c.id AND r.user_id = u.id"
                    + "  ORDER BY created DESC;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                int rid = rs.getInt("id");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rs.getString("created"));
                int customerId = rs.getInt("customer_id");
                String customerName = rs.getString("customer_name");
                String customerTel = rs.getString("customer_tel");
                int userId = rs.getInt("user_id");
                String userName = rs.getString("user_name");
                String userTel = rs.getString("user_tel");
                double total = rs.getDouble("total");
                Receipt receipt = new Receipt(rid, created,
                        new User(userId,userName,userTel),new Customer(customerId,customerName,customerTel));
                
                getReceiptDetail(conn, id, receipt);
                //list.add(receipt);
                return receipt;
            }
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            System.out.println("Can't open database! :(");
            System.exit(0);
        } catch (ParseException ex) {
            System.out.println("Date error "+ex);
        }
        db.close();
        return null;
    }

    private void getReceiptDetail(Connection conn, int id, Receipt receipt) throws SQLException {
        String sqlDetail = "SELECT rd.id as id,\n"
                + "       receipt_id,\n"
                + "       product_id,\n"
                + "       p.name as product_name,\n"
                + "       p.price as product_price, \n"
                + "       rd.price as price,\n"
                + "       amount\n"
                + "  FROM receipt_detail rd, product p\n"
                + "  WHERE receipt_id = ? AND rd.product_id = p.id;"
                + "  ORDER BY created DESC;";
        PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
        stmtDetail.setInt(1, id);
        ResultSet resultDetail = stmtDetail.executeQuery();
        while(resultDetail.next()) {
            int receiveId = resultDetail.getInt("id");
            int productId = resultDetail.getInt("product_id");
            String productName = resultDetail.getString("product_name");
            double productPrice = resultDetail.getDouble("product_price");
            double price = resultDetail.getDouble("price");
            int amount = resultDetail.getInt("amount");
            Product product = new Product(productId, productName, productPrice);
            receipt.addReceiptDetail(receiveId,product, amount, price);
        }
    }

    @Override
    public int delete(int id) {
        PreparedStatement stmt = null;
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "DELETE FROM receipt WHERE id = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
            stmt.close();

        } catch (SQLException ex) {
            System.out.println("Can't open database! :(");
            System.exit(0);
        }
        db.close();
        return row;
    }

    @Override
    public int update(Receipt object) {/*
        PreparedStatement stmt = null;
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE product SET name = ?,price = ? WHERE id = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            stmt.setInt(3, object.getId());
            row = stmt.executeUpdate();
            System.out.println("Affect row : " + row);
            System.out.println("-----------------------");

            stmt.close();
        } catch (SQLException ex) {
            System.out.println("Can't open database! :(");
            System.exit(0);
        }
        db.close();
        return row;*/
        return 0;
    }

    public static void main(String[] args) {
        Product p1 = new Product(1,"Espresso 3",45);
        Product p2 = new Product(2,"Americano",60);
        User seller = new User(1,"Patcharawlai Cha-dathong","0615416798");
        Customer customer = new Customer(1,"Bright","0936697582");
        Receipt receipt = new Receipt(seller,customer);
        receipt.addReceiptDetail(p1, 1);
        receipt.addReceiptDetail(p2, 3);
        System.out.println(receipt);
        ReceiptDao dao = new ReceiptDao();
        System.out.println(dao.getAll());
        System.out.println("id = "+dao.add(receipt));
        System.out.println("Get id = "+dao.get(1));
        System.out.println("Receipt after add: "+receipt);
        System.out.println("Get all: "+dao.getAll());
        
        Receipt newReceipt = dao.get(receipt.getId());
        System.out.println(receipt.getId());
        System.out.println("New Recceipt: "+newReceipt);
        
        dao.delete(receipt.getId());
    }
}

