/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patcharawalai.stroeproject.poc;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.Product;
import model.User;

/**
 *
 * @author Patcharawalai
 */
public class TestInsertCustomer {

    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
           String sql = "INSERT INTO customer (name,tel) VALUES (?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            Customer customer = new Customer(-1,"Ploy","0878859985");
            stmt.setString(1, customer.getName());
            stmt.setString(2, customer.getTel());
            int row = stmt.executeUpdate();
            System.out.println("Affect row" + row);
            ResultSet result = stmt.getGeneratedKeys();
            int id = -1;
            if(result.next()){
                id = result.getInt(1);
            }
             System.out.println("Affect row: " + row + " id: " + id);

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
    }
}
